# from email import header
# from turtle import pos, title
# from wsgiref import headers
import requests
import numpy
import time
import datetime
import os
import json
from lxml import etree
from bs4 import BeautifulSoup as bs4
import csv
import pandas as pd
import urllib.parse

from components.io import requests_from_url
from components.io import write_file
from components.io import read_file
from components.io import convert_XML2JSON

from process_address import extract_address_toList
from process_address import reformat_address

# https://www.hongkongairport.com/flightinfo-rest/rest/flights?span=1&date=2021-12-24&lang=en&cargo=false&arrival=false
global URLString, header

header = {"Cookie": "acw_tc=0bc1a04c16432904101565817edf561f64aa92c37f189ee5ff6055959765b8",
          "Content-Type": "text/html; charset=utf-8",
          "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:50.0) Gecko/20100101 Firefox/50.0",
          "Connection": "keep-alive",
          'Host': 'www.openrice.com',
          "Cache-Control": "private, max-age=0",
          }

# https://blockcast.it/page/1/?s=DOGE
URLString = {
    # "base": "https://blockcast.it/page/",
    # "query": "/?s="
    "base": "https://www.openrice.com/en/hongkong/restaurants/district/",
    "query": "tsim-sha-tsui"
}

DATETOQUERY = ["2021-12-25", "2021-12-26", "2021-12-27"]
# DATETOQUERY = ["2021-12-25", "2021-12-30"]

URLLIST = []


def date_to_URL(date):
    finalURL = URLString["prefix"] + \
        str(date) + URLString["cargo"] + "true" + URLString["arrival"] + "true"
    return finalURL


def download_files():
    res = requests_from_url(URLString["RL_S"], "xml")
    # convert_XML2JSON(res)


allPosts = []

csvHeader = ['articleTitle', 'publishDate', 'author', 'articleURL']


def write_CSV_header():
    with open('../data/doge_articles.csv', 'w', encoding='UTF8') as f:
        writer = csv.writer(f)

        # write the header
        writer.writerow(csvHeader)
        f.close()


def write_CSV_row(data):
    with open('../data/doge_articles.csv', 'w', encoding='UTF8') as f:
        writer = csv.writer(f)

        # write the data
        writer.writerow(data)
        f.flush()
        f.close()


def get_all_the_posts_in_page(soup):
    # print(CSSClass)
    posts = soup.find_all("div", class_="grid-text")
    print(len(posts))

    csvData = []

    for post in posts:
        articleTitle = post.find("h3").text
        articleURL = post.find("h3").find("a")["href"]
        author = (post.find("div", class_="grid-date")).find("a").text
        publishDate = (post.find("div", class_="grid-date")
                       ).find("span", class_="date").text

        rowData = (",").join(
            [articleTitle, publishDate, author, articleURL]
        )
        # rowData = rowData+"\n"

        jsonData = {
            "articleTitle": articleTitle,
            "author": author,
            "publishDate": publishDate,
            "articleURL": articleURL
        }

        # print(jsonData)
        csvData.append(rowData)
        allPosts.append(jsonData)

    return(csvData)

    # print(posts)

    # return posts


def get_title(soup, HTMLele="div", CSSClass=""):
    titlehtml = soup.find(HTMLele)
    return [title.text, titlehtml]


def get_date(soup, HTMLele="div", CSSClass=""):
    # grid-date
    date = soup.find(HTMLele, class_=CSSClass)
    date = date.find("span", class_="date").text

    return date


def get_article_URL(soup, HTMLele="div", CSSClass=""):
    URL = soup.find("a")["href"]

    return URL


global CSS_Class

CSS_Class = {
    "restObject": ["sr1-listing-content-cell pois-restaurant-list-cell"],
    "title": ["h2"],
    "address": ["icon-info address", "span"],
    "price": ["icon-info icon-info-food-price", "span"],
    "restType": ["icon-info icon-info-food-name", "li"],
    "review": ["counters-container", "span"],
    "like": ["emoticon-container smile-face pois-restaurant-list-cell-content-right-info-rating-happy", "span"],
    "dislike": ["emoticon-container sad-face pois-restaurant-list-cell-content-right-info-rating-sad", "span"]
}


def getting_rest_summary(rest):
    """

        title: string
        address: string
        price: string
        restType: 

        optional:
        like: number;
        review: number;
        dislike: number

    """

    print(CSS_Class["review"][-1])

    title = ""
    review = ""
    address = ""
    restType = ""
    price = ""
    like = ""
    dislike = ""

    title = rest.find(CSS_Class["title"][0]).text

    review = rest.find("div", class_=CSS_Class["review"][0]).find(
        CSS_Class["review"][-1]).text

    address = rest.find("div", class_=CSS_Class["address"][0]).find(
        CSS_Class["address"][-1]).text

    price = rest.find("div", class_=CSS_Class["price"][0]).find(
        CSS_Class["price"][-1]).text

    restType = rest.find("div", class_=CSS_Class["restType"][0]).find(
        CSS_Class["restType"][-1]).text

    review = rest.find("div", class_=CSS_Class["review"][0]).find(
        CSS_Class["review"][-1]).text

    like = rest.find("div", class_=CSS_Class["like"][0]).find(
        CSS_Class["like"][-1]).text

    dislike = rest.find("div", class_=CSS_Class["dislike"][0]).find(
        CSS_Class["dislike"][-1]).text

    restObj = {
        "title": title,
        "review": review,
        "address": address,
        "restType": restType,
        "price": price,
        "like": like,
        "dislike": dislike,
        "review": review,
    }

    print(restObj)

    return restObj


global restTST
restTST = []


def scraping_data(url, keyword):

    counter = 15
    isLastPage = False

    for page in range(99999999):

        try:
            reqURL = url[0] + keyword + "?page=" + str(counter)
            print(reqURL)

            page = requests.get(reqURL, headers=header)
            # page = urllib.parse.quote(reqURL)
            soup = bs4(page.text, 'html.parser')

            # print(page.text)

            # assuming page 1-100

            if isLastPage is True:
                break
            else:
                # counter += 1
                if counter > 16:
                    isLastPage = True

                # print(soup.find("a", class_="pagination-button next js-next"))

                if soup.find("a", class_="pagination-button next js-next") is not None:
                    print("srcaping page {} data".format(counter))

                    # restList = soup.find_all(
                    #     "li", class_="sr1-listing-content-cell pois-restaurant-list-cell")

                    # print(restList)

                    for rest in soup.find_all("li", class_="sr1-listing-content-cell pois-restaurant-list-cell"):
                        # title = rest.find("h2").text
                        # print(title)

                        # review = rest.find(
                        #     "div", class_="counters-container").find("span").text

                        # print(review)

                        restTST.append(getting_rest_summary(rest))

                        # print(rest.find_all(
                        #     "img", class_="promotion-logo-image")[-1])

                        # if rest.find_all("img", class_="promotion-logo-image")[-1]["src"].find("TypeA") > -1:
                        #     vBubble = 1  # A

                        # if rest.find("img", class_="promotion-logo-image")[-1]["src"].find("TypeB") > -1:
                        #     vBubble = 2  # B

                        # if rest.find("img", class_="promotion-logo-image")[-1]["src"].find("TypeC") > -1:
                        #     vBubble = 3  # C

                        # if rest.find("img", class_="promotion-logo-image")[-1]["src"].find("TypeD") > -1:
                        #     vBubble = 4  # D

                        # print(vBubble)

                    # data = get_all_the_posts_in_page(soup)
                    counter += 1
                else:
                    print("srcaping the last page")
                    isLastPage = True
                    counter += 1
                    # page 100

            # if counter > 100:
            #     break
        except:
            print("cannot connect to the server")
            # break


def main():
    scraping_data([URLString["base"]], "tsim-sha-tsui")
    write_file("../data", "rest_TST.json", restTST)


if __name__ == '__main__':
    main()
