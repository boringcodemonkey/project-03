from turtle import position
import requests
import numpy
import time
import datetime
import os
import json
from lxml import etree
import re

from components.io import requests_from_url
from components.io import write_file
from components.io import convert_XML2JSON
from components.io import writeTxTFile

from components.convertHK1980toWGS84 import convertHK1980toWGS84

global URLString
URLString = {
    "RL_S": "https://www.fehd.gov.hk/english/licensing/license/text/LP_Restaurants_EN.XML",
    "GovMap": {
        # https://www.map.gov.hk/gih-ws2/search?keyword={"29 TAI O MARKET STREET"}#0
        "prefix": "https://www.map.gov.hk/gih-ws2/search?keyword=",
        "suffix": "#0"
    }

}

headers = {
    "Content-Type": "application/json",
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"
    # "Connection": "close"
}


def extract_address_toList(data):
    list = []
    for obj in data:
        # print(obj)
        list.append(obj["ADR"])
    writeTxTFile("../data/", "d17_ADR_only.txt", list)


def contain_words(addr, word):
    if re.search(word.upper(), addr.upper()) is not None:
        return True
    else:
        return False


def contain_Street(addr):
    if re.search("STREET", addr.upper()) is not None:
        return True
    else:
        return False


def contain_ROAD(addr):
    if re.search("ROAD", addr.upper()) is not None:
        return True
    else:
        return False


def clean_address(addr, addrType):
    position = 0

    # if addrType.upper() == "ROAD":
    addrSplit = addr.split(",")
    addrSplit = [ele.upper().strip(" ") for ele in addrSplit]
    # print("\n\n", addrSplit)

    # SHOP NO. 34, G/F., YAT TUNG SHOPPING CENTRE, YAT TUNG ESTATE, 8 YAT TUNG STREET, TUNG CHUNG, HONG KONG
    # addrSplit value : ["SHOP NO. 34", "G/F.", "YAT TUNG SHOPPING CENTRE", "YAT TUNG ESTATE", "8 YAT TUNG STREET", "TUNG CHUNG", "HONG KONG"]

    for ele in addrSplit:
        if addrType in ele:
            if re.search(r'\d', ele) is not None:
                addrSplit[position] = ele[re.search(r'\d', ele).start():]
                addrCleaned = ', '.join(addrSplit[position:])
                return addrCleaned
                break
            else:
                addrCleaned = ', '.join(addrSplit[position:])
                return addrCleaned
                break
        else:
            position += 1


def get_HK1980coords(addr):
    print("\naddr to query: {}".format(addr))

    url = URLString["GovMap"]["prefix"] + addr + URLString["GovMap"]["suffix"]
    url = url.replace(" ", "%20")

    try:
        res = requests.get(url, headers=headers).json()[0]

        print("res result: {}".format(res))

        HK1980List = {
            "easting": res["easting"],
            "northing": res["northing"]
        }
    except:
        print("Cannot connect to the API server")
        print(url)

        HK1980List = {
            "easting": 0,
            "northing": 0
        }

    print("\n\nHK1980: {}", format(res))

    # return HK1980List

    return [res["easting"], res["northing"]]


def reformat_address(addr):
    # GPSdata = {}
    GPSdata = ""
    try:
        if contain_words(addr, "STREET"):
            # print("cleann the address now")
            GPSdata = convertHK1980toWGS84(
                get_HK1980coords(clean_address(addr, "STREET")))

        if contain_words(addr, "ROAD"):
            # print("cleann the address now")
            GPSdata = convertHK1980toWGS84(
                get_HK1980coords(clean_address(addr, "ROAD")))

        if contain_words(addr, "PEI"):
            # print("cleann the address now")
            GPSdata = convertHK1980toWGS84(
                get_HK1980coords(clean_address(addr, "PEI")))
    except:
        print("we cannot process this address, the address format is not in ROAD, STREET or PEI")
        GPSdata = {
            "WSG84": {
                "lat": 0,
                "lng": 0
            },
            "EPSG2326": {
                "easting": 0,
                "northing": 0
            }
        }

    return GPSdata
